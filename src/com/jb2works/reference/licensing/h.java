//Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: fieldsfirst safe 
//Source File Name:   source

package com.jb2works.reference.licensing;

import com.jb2works.reference.HttpScanner;
import com.jb2works.reference.e;
import com.jb2works.reference.page.g;
import com.jb2works.reference.util.m;
import com.jb2works.reference.util.o;
import com.jb2works.reference.util.p;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

//Referenced classes of package com.jb2works.reference.licensing:
//         d, e, c, f

public class h
{

 private d l;
 private boolean t;
 private final String o = com.jb2works.reference.util.p.a("d01ff0953bdb312d8e1a2d082ed6", "xyz");
 private final String b = com.jb2works.reference.util.p.a("0faee10117eabdd410e5ba", "xyz");
 private final String m = com.jb2works.reference.util.p.a("0b07544bcf918c65113c6011cc5a29", "xyz");
 private final String u = com.jb2works.reference.util.p.a("4eaef005ff8af0ce5efaaf800ee121fef408f5", "xyz");
 private final String d = com.jb2works.reference.util.p.a("3cc8653bc2344c4c34fbe6b43bec204b51d44c22741c5480fc91645b49c3", "xyz");
 private final String n = com.jb2works.reference.util.p.a("99a3aaf9d636fd392bda3eaaa83693b2599449b9a3", "xyz");
 private final String g = com.jb2works.reference.util.p.a("614897015c893808253049111a48161149131d249c5c", "xyz");
 private final String q = com.jb2works.reference.util.p.a("19d96e727aa28765e19663e49672a796682797a1777e", "xyz");
 private final String i = com.jb2works.reference.util.p.a("b99731c9c646f9c9a509c3c6b9c7d59ade6aa9d36f", "xyz");
 private final String k = com.jb2works.reference.util.p.a("96470a87484d80199c5b18", "xyz");
 private final d f;
 private final d v;
 private final d c;
 private final d h;
 private final d a;
 private final d p;
 private final d s;
 private final d j;
 private final d r;
 public final d e[];

 public h()
 {
     l = null;
     t = false;
     f = b(o + b);
     v = b(o + m);
     c = b(o + u);
     h = b(o + d);
     a = b(o + n);
     p = a(o + g);
     s = a(o + q);
     j = a(o + i);
     r = a(o + k);
     e = (new d[] {
         f, v, c, h, a, p, s, j, r
     });
 }

 public boolean g()
 {
     if(l == null)
         a(c(d(k())));
     boolean ret=l.a();
     return true;
 }

 private d c(d d1)
 {
     if((d1 == s || d1 == j) && a(d1.g()))
         d1 = c;
     return d1;
 }

 private boolean a(long l1)
 {
     return b(l1) < 0L;
 }

 public int j()
 {
     if(l != s && l != j)
         return -1;
     else
         return d(l.g());
 }

 private int d(long l1)
 {
     return (int)(b(l1) / 0x36ee80L);
 }

 private long b(long l1)
 {
     long l2 = l1 + com.jb2works.reference.e.z().E();
     l2 -= System.currentTimeMillis();
     return l2;
 }

 private d d(d d1)
 {
     if(d1 == p && f(d1.b()))
         d1 = v;
     return d1;
 }

 public static boolean f(long l1)
 {
     return c(l1) < 0L;
 }

 private d k()
 {
     d d1 = null;
     try
     {
         boolean flag = false;
         com.jb2works.reference.licensing.e e1 = com.jb2works.reference.licensing.d.a(false);
         d1 = e1 != null ? e1.b : null;
     }
     catch(IOException ioexception) { }
     catch(RuntimeException runtimeexception) { }
     return d1 != null ? d1 : f;
 }

 private d a(String s1)
 {
     return com.jb2works.reference.licensing.d.a("registerAfter.xml", s1, true);
 }

 private d b(String s1)
 {
     return com.jb2works.reference.licensing.d.a("registerBefore.xml", s1, false);
 }

 public boolean b(d d1)
 {
     return d1 == l;
 }

 public void a(HttpScanner httpscanner, com.jb2works.reference.page.e e1)
 {
     a(e1.b.b, e1.a.b);
     c(e1.c.b);
     t = !f();
     if(t)
         return;
     m m1 = com.jb2works.reference.e.z().aw;
     String s1 = m1.g();
     String s2 = m1.b();
     if(com.jb2works.reference.e.z().M() == null)
     {
         c c1 = com.jb2works.reference.licensing.c.b(s1);
         if(c1 != null && c1.a())
         {
             a(v);
         } else
         {
             if(c1 != null)
                 p.b(c1.b());
             a(p);
         }
     } else
     {
         f f1 = com.jb2works.reference.licensing.f.c(com.jb2works.reference.e.z().M());
         if(f1 == null)
             a(a);
         else
         if(f1.a(s1, s2))
             a(r);
         else
         if(!f1.a())
             a(h);
         else
         if(l == c)
         {
             j.c();
             a(j);
         } else
         {
             s.c();
             a(s);
         }
     }
 }

 private void a(d d1)
 {
     if(l != d1)
     {
         l = d1;
         try
         {
             d1.i();
         }
         catch(IOException ioexception)
         {
             ((Throwable) (ioexception)).printStackTrace();
         }
     }
 }

 static URL d()
     throws MalformedURLException
 {
     String s1 = com.jb2works.reference.e.z().l();
     int i1 = s1.indexOf("http://") + "http://".length();
     i1 = s1.indexOf('/', i1 + 1);
     return new URL(s1.substring(0, i1));
 }

 public boolean f()
 {
     try
     {
         return com.jb2works.reference.util.o.a(d()) != null;
     }
     catch(MalformedURLException malformedurlexception)
     {
         ((Throwable) (malformedurlexception)).printStackTrace();
     }
     return false;
 }

 public String h()
 {
     String s1 = com.jb2works.reference.e.z().d();
     return s1 != null && s1.indexOf("<") == -1 ? s1 : "&lt;no proxy>";
 }

 private static void a(String s1, String s2)
 {
     s1 = !"<no proxy>".equals(((Object) (s1))) && !"".equals(((Object) (s1))) ? s1 : null;
     s2 = !"<no port>".equals(((Object) (s2))) && !"".equals(((Object) (s2))) ? s2 : null;
     com.jb2works.reference.e.z().a(s1, s2);
 }

 public String c()
 {
     String s1 = com.jb2works.reference.e.z().I();
     return s1 != null && s1.indexOf("<") == -1 ? s1 : "&lt;no port>";
 }

 public String i()
 {
     String s1 = com.jb2works.reference.e.z().M();
     return s1 != null && s1.indexOf("<") == -1 ? s1 : "&lt;no activation key>";
 }

 private void c(String s1)
 {
     String s2 = !"<no activation key>".equals(((Object) (s1))) && !"".equals(((Object) (s1))) ? s1 : null;
     com.jb2works.reference.e.z().d(s2);
 }

 public String b()
 {
     if(p == l)
         return com.jb2works.reference.util.p.a("791c8225436f7013cd681a6a1486", "ldsf");
     if(s == l || j == l)
         return com.jb2works.reference.util.p.a("e1d36e6f22c40e6162f03c626de9e17d61fc387172323f156060", "sedf");
     if(r == l || j == l)
         return com.jb2works.reference.util.p.a("0f231006dafd333f03eef9524409d0298244024023d0ba", "sedf1");
     else
         return "n/a";
 }

 public int e()
 {
     if(p == l)
         return e(l.b());
     else
         return -1;
 }

 static int e(long l1)
 {
     return (int)(c(l1) / 0x36ee80L);
 }

 private static long c(long l1)
 {
     long l2 = l1 + com.jb2works.reference.e.z().r();
     l2 -= System.currentTimeMillis();
     return l2;
 }

 public boolean a()
 {
     return t;
 }

 d d(String s1)
 {
     for(int i1 = 0; i1 < e.length; i1++)
     {
         d d1 = e[i1];
         if(d1.d().equals(((Object) (s1))))
             return d1;
     }

     return null;
 }
}
